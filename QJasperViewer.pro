#-------------------------------------------------
#
# Project created by QtCreator 2013-02-22T13:17:07
#
#-------------------------------------------------

QT       += core gui sql xml xmlpatterns

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qjasperviewer
TEMPLATE = app

win32-* {
INCLUDEPATH += "C:\\Programs\\Java\\jdk1.6.0_41\\include"
INCLUDEPATH += "C:\\Programs\\Java\\jdk1.6.0_41\\include\\win32"
LIBS += -L"C:\\Programs\\Java\\jdk1.6.0_41\\lib" -ljvm
}
linux-* {
INCLUDEPATH += /usr/lib/jvm/default-java/include
INCLUDEPATH += /usr/lib/jvm/default-java/include/linux
LIBS += -L/usr/lib/jvm/default-java/jre/lib/i386/server -ljvm -ljsig

target.path = /usr/bin
INSTALLS += target
data.path = /usr/share/qjasperviewer
data.files = dist/*
INSTALLS += data
}

SOURCES += main.cpp\
        mainwindow.cpp \
    settingsdlg.cpp \
    aboutdlg.cpp \
    qjasper.cpp \
    parameterdlg.cpp \
    XMLSerializer.cpp \
    jrxmldocument.cpp

HEADERS  += mainwindow.h \
    settingsdlg.h \
    aboutdlg.h \
    qjasper.h \
    parameterdlg.h \
    Serializable.h \
    Serializer.h \
    XMLSerializer.h \
    jrxmldocument.h

FORMS    += mainwindow.ui \
    settingsdlg.ui \
    aboutdlg.ui \
    parameterdlg.ui

OTHER_FILES +=

RESOURCES += \
    qjasperviewer.qrc
