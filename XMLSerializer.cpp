/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */

//// Acknowledgement
// Filename		: XMLSerializer.cpp
// Author		: Siddharth Barman
// Date			: Aug 06 2005
// Description	: Implementation file for 

//------------------------------------------------------------------------------

#include "XMLSerializer.h"

#include <QDebug>
#include <QFile>
#include <QXmlQuery>

typedef QString _bstr_t;
//------------------------------------------------------------------------------

CXMLSerializer::CXMLSerializer()
{
    // QDomProcessingInstruction header = m_doc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\"" );
    // m_doc.appendChild( header );
}
//------------------------------------------------------------------------------

CXMLSerializer::CXMLSerializer(const QString& sFile, const QString& sAppName,
							   bool bLoad)
{
    // QDomProcessingInstruction header = m_doc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\"" );
    // m_doc.appendChild( header );

    try
    {
        // m_doc.CreateInstance(CLSID_DOMDocument26);
        SetFile(sFile);
        SetApplicationName(sAppName);

        if (bLoad)
        {
            // read in the xml file
            ReadFile();
        }
        else
        {
            // create a new xml structure
            QDomElement rootnode = CreateChildNode(m_sAppName);
            QDomNode as = m_doc.appendChild(rootnode);
            Q_ASSERT(!as.isNull());
        }
    }
    catch(...)
    {
        qDebug() << UNSPEC_ERROR;
    }
}
//------------------------------------------------------------------------------

 CXMLSerializer::~CXMLSerializer()
{
    if (!m_sFile.isEmpty())
    {
        QFile file(m_sFile);

        if (!file.open(QIODevice::WriteOnly))
            throw CXMLException("Failed to write file");

        file.write(QByteArray(m_doc.toString().toUtf8()));
        file.flush();
        file.close();
	}

    m_doc.clear();
}
//------------------------------------------------------------------------------

void CXMLSerializer::ReadFile()
{	
    Q_ASSERT(m_sFile.isEmpty() == FALSE);

    QFile file(m_sFile);
    QString errMsg;
    int errLine, errColumn;

    if (!file.open(QIODevice::ReadOnly))
        throw CXMLException("File not found");

    if (!m_doc.setContent(&file, &errMsg, &errLine, &errColumn))
        throw CXMLException(errMsg);

    file.close();
}
//------------------------------------------------------------------------------

void CXMLSerializer::SetFile(const QString& sValue)
{
	m_sFile = sValue;
}
//------------------------------------------------------------------------------

QString CXMLSerializer::GetFile()
{
	return m_sFile;
}
//------------------------------------------------------------------------------

void CXMLSerializer::SetApplicationName(const QString& sValue)
{
	m_sAppName = sValue;
}
//------------------------------------------------------------------------------

QString CXMLSerializer::GetApplicationName()
{
	return m_sAppName;
}
//------------------------------------------------------------------------------

// Returns the plural form of a word e.g. cat becomes cats
QString CXMLSerializer::GetPlural(const QString& sWord)
{
    QString sResult;
    QChar ch = sWord.at(sWord.size()-1);
	
    if (ch == 's' || ch == 'S')
	{
		// e.g. address become address[e]s
        sResult = QString("%1es").arg(sWord);
	}
    else if (ch == 'y' || ch == 'Y')
	{
		// e.g. city becomes cities
        sResult = QString("%1ies").arg(sWord);
	}
	else
	{
		// e.g. dog becomes dogs
        sResult = QString("%1s").arg(sWord);
	}
	return sResult;
}

//------------------------------------------------------------------------------
bool CXMLSerializer::SerializeObject(ISerializable* obj, 
                                     QDomNode parent)
{
	// To serialize the object correctly, we need to get its identity and then 
	// get the property list after which we query for value of each property.
	// We also need to check if the XML file has a node for this object, this 
	// will be done based on the ID of the object <object id="xxx">. 

    Q_ASSERT(obj != NULL);
    if (obj == NULL)
		return false;

    QString sClass	= obj->GetClassName();			// e.g. <contact>
    QString sId		= obj->GetID();					// e.g. <contact id="1">
	bool	bMulti	= obj->HasMultipleInstances();	// if multiple instances has
													// to be stored

    QDomProcessingInstruction header = m_doc.createProcessingInstruction( "xml", "version=\"1.0\" encoding=\"UTF-8\"" );
    parent.appendChild(header);
	
	// lets check if the object has already been persisted
    // _bstr_t bsXPath;
    QString sPath;
    QString sOuterNodeName;
    QDomNode node;
    QDomNode refnode = parent;

    if (bMulti)
	{
		// do some english lang processing for getting the plural form
		sOuterNodeName = GetPlural(sClass);
		
		// e.g. <contacts>
		//			<contact>		
        if (parent.isNull())
		{
			// reference is the root			
            sPath = QString("//%1/%2/%3[id='%4']")
                    .arg(m_sAppName).arg(sOuterNodeName).arg(sClass).arg(sId);
		}
		else
		{
			// reference is the parent node
            sPath = QString("%1s/%2[id='%3']").arg(sOuterNodeName).arg(sClass).arg(sId);
		}
	}
	else
	{
		// e.g. <contact> 
        if (parent.isNull())
		{
			// reference is the root
            sPath = QString("//%1/%2[id='%3']").arg(m_sAppName).arg(sClass).arg(sId);
		}
		else
		{
			// reference is the parent node
            sPath = QString("%1[id='%2']").arg(sClass).arg(sId);
		}
	}

    QDomNode rootnode = m_doc.firstChild();
    QDomNode outernode; // e.g. <contacts>

    if (refnode.isNull())
	{
		// since no reference node is present, the reference node is the 
		// root node.
		refnode = rootnode;
	}
	
    if (refnode.isNull())
	{
		// this is probably a brand new document without any contents
		refnode = CreateChildNode(m_sAppName, rootnode);
        m_doc.appendChild(refnode);
	}

    Q_ASSERT (!refnode.isNull());

    node =  selectSingleNode(refnode, sPath);

    if (node.isNull())
	{
		// it is possible that the parent node e.g. <contacts> itself is
		// not there. We need to create it.
        if (bMulti)
		{
            QString sOuterNodeName;

            sOuterNodeName = sClass + "s"; // e.g. contact+s = contacts
            sPath = QString("//%1/%2").arg(m_sAppName).arg(sOuterNodeName);
			
            outernode = selectSingleNode(refnode, sPath);

            if (outernode.isNull())
			{
				// this node does not exist, we need to create it				
				outernode = CreateChildNode(sOuterNodeName, rootnode);
                refnode.appendChild(outernode);
			}			
		}
	}
	
    QStringList props;
	CProperty	property;
    QString		sProperty;
    QString		sValue;
	int			nProps;
	int			nIndex;
	
	nProps = obj->GetProperties(props); // property count

	for(nIndex = 0; nIndex < nProps; nIndex++)
	{
        sProperty	= props.at(nIndex);
		property.SetName(sProperty);

        if (obj->GetPropertyValue(sProperty, property))
		{
			// create the node if NULL
            if (node.isNull())
			{
				// this is the entity node e.g. <contact>
                if (bMulti == false)
				{					
					node = CreateChildNode(sClass, refnode);
				}
				else
				{
					node = CreateChildNode(sClass, outernode);
				}

				// set the ID attribute
                if (!sId.isEmpty())
				{
                    SetAttributeValue(node, "ID", sId);
				}
			}

			switch(property.GetType())
			{
			case Simple:
				sValue = property.GetStringValue();				
				SetProperty(node, property); // persist sValue in the xml
				break;

			case SimpleList:
				{
					// first we create the outer node e.g. <phonenumbers>
                    QString sOuterNodeName = GetPlural(sProperty);
                    QStringList* values = &property.GetStringList();
                    QDomNode outernode;
			
					// attach the outernode
					outernode = CreateChildNode(sOuterNodeName, node);
                    node.appendChild(outernode);
					
					// iterate through all the list of values and store them
                    for(int nIndex = 0; nIndex < values->size(); nIndex++)
					{
                        sValue = values->at(nIndex);
						CProperty singleprop(sProperty, sValue);
						SetProperty(outernode, singleprop, true); // true means
						// we want a new node to be created		
					}				
				}
				break;
			
			case Complex:				
				SerializeObject(property.GetObject(), node);
				break;
			
			case ComplexList:
				{
					// first we create the outer node e.g. <phonenumbers>
                    QString sOuterNodeName = GetPlural(sProperty);
					CPtrList* values = &property.GetObjectList();
                    QDomNode outernode;
			
					// attach the outernode
					outernode = CreateChildNode(sOuterNodeName, node);
                    node.appendChild(outernode);
					
					// iterate through all the list of values and store them
                    for(int nIndex = 0; nIndex < values->size(); nIndex++)
					{
                        ISerializable* pObject = (ISerializable*)values->at(nIndex);
						CProperty singleprop(sProperty, pObject);
						SerializeObject(pObject, outernode);							
					}				
				}
				break;
			}			
		}
	}

    if (!m_sFile.isEmpty())
    {
        QFile file(m_sFile);

        if (!file.open(QIODevice::WriteOnly))
        {
            throw CXMLException("Failed to write file");
            return false;
        }

        file.write(QByteArray(m_doc.toString().toUtf8()));
        file.flush();
        file.close();
    }
	return true;
}
//------------------------------------------------------------------------------

// Serializes a single object.
bool CXMLSerializer::Serialize(ISerializable* obj)
{
    return SerializeObject(obj);
}
//------------------------------------------------------------------------------

// Serialized a list of objects.
bool CXMLSerializer::Serialize(const CPtrList& objects)
{
	bool	 bResult = true;

    CPtrList::const_iterator iter = objects.begin();

    while(iter != objects.end() && bResult)
	{
        ISerializable* object = (ISerializable*) iter;
		bResult = SerializeObject(object);
        ++iter;
	}
	
	return bResult;
}
//------------------------------------------------------------------------------

// Returns number of items deserialized. In case of fatal error, returns 
// DESERIALIZATION_FAIL. Created objects are added to the objList
int CXMLSerializer::DeserializeObject(IObjectFactory* factory, CPtrList& objList, 
                                      QDomNode parent)
{
    Q_ASSERT(factory != NULL);
    if (NULL == factory)
	{
		return DESERIALIZATION_FAIL;
	}

	// create an empty instance of the object to de-serializable
	ISerializable* pEmptyObject = factory->Create(); // this needs to to be
													 // destroyed

    Q_ASSERT(pEmptyObject != NULL);
    if (NULL == pEmptyObject)
	{		
		factory->Destroy(pEmptyObject);
		return DESERIALIZATION_FAIL;
	}

    QString		sClass = pEmptyObject->GetClassName();
    QString		sPath;
    QString		sId;
    QString		sPropName;
    QString		sPropValue;
    QString		sPropType;
    QStringList sProps;
	int			nProperties;
	int			nObjects = 0;
		
	nProperties = pEmptyObject->GetProperties(sProps);

    if (nProperties == 0)
	{
        qDebug() << "Zero properties for Class:" << sClass;
		factory->Destroy(pEmptyObject);
		return 0;
	}

	bool		bMulti = pEmptyObject->HasMultipleInstances();
	_bstr_t		bsXPath;

    if (bMulti)
	{
		// e.g. <contacts>
		//			<contact>
        if (parent.isNull())
		{
			// reference is root
            sPath = QString("//%1/%2s/%3").arg(m_sAppName).arg(sClass).arg(sClass);
		}
		else
		{
			// reference is parent node
            sPath = QString("%1s/%2").arg(sClass).arg(sClass);
		}
	}
	else
	{
		// e.g. <contact> 
        if (parent.isNull())
		{
			// reference is root
            sPath = QString("//%1/%2/%3").arg(m_sAppName).arg(sClass).arg(sId);
		}
		else
		{
			// reference is parent node
            sPath = QString("%1").arg(sClass);
		}
	}

    QDomNode	  refnode;
    QDomNodeList  nodes;

    if (parent.isNull())
	{
        refnode = m_doc.firstChild();
	}
	else
	{
		refnode = parent;
	}

    if (refnode.isNull())
	{
		return 0; // even the parent node does not exist
				  // This is not an error, all it means is that
				  // no such objects have been deserialized
	}

    nodes =  selectNodes(refnode, sPath);
	
    if (nodes.isEmpty())
	{
        qDebug() << "\nNo nodes for XPATH: " << sPath;
		factory->Destroy(pEmptyObject);
		return 0;
	}
	
	// loop for iterating over the class nodes e.g. <contact>
    for (int nIndex = 0; nIndex < nodes.size(); nIndex++)
	{
        QDomNode node = nodes.at(nIndex);

        if (node.isNull())
		{
			ISerializable* pObj = factory->Create();
		
            GetAttributeValue(node, "ID", sId); // get the ID

			// loop through all the properties
			for(int nPropIndex = 0; nPropIndex < nProperties; nPropIndex++)
			{
                sPropName = sProps.at(nPropIndex);
			
				CProperty property(sPropName);

                if (pObj->GetPropertyValue(sPropName, property))
				{
					switch(property.GetType())
					{
					case Simple:						
                        if (GetProperty(node, property))
						{					
							pObj->SetPropertyValue(sPropName, property);
						}
						break;

					case SimpleList:
                        if (GetProperty(node, property))
						{
							pObj->SetPropertyValue(sPropName, property);
						}

						break;

					case Complex:
						{
						// get a list of new objects for the complex object at hand
						CPtrList list;
						DeserializeObject(property.GetFactory(), list, node);

						// note: newly created object are present in the list. These object
						// will be destroyed by the outer class which will accept these 
						// objects as a property-value. In case the outer class wants to
						// hold on to these objects, they can do so and destroy them later.
						// The outer object class might also decide to immediately destroy
						// the passed in object. This is true only for Complex types.
						
						// iterate through the complex objects and add them to the previous
						// (parent) object
                        for(int n=0; n < list.size(); n++)
						{
							CProperty newproperty;
							newproperty.SetFactory(property.GetFactory());
                            newproperty = (ISerializable*)list.at(n);
							
							// update the parent
							// the parent is now responsible for the object lifetime
							pObj->SetPropertyValue(sPropName, newproperty); 																	
						}					
						}
						break;

					case ComplexList:
						{
						// first locate the outernode, then walk over the subnodes
						// get a list of new objects for each complex object at hand
                        QString sOuterNode = GetPlural(sPropName);
                        QDomNodeList subnodes;
                        QDomNode complexnode;

                        subnodes =  selectNodes(node, sOuterNode);

                        if (subnodes.isEmpty())
						{
							break;
						}
						
                        for(int n = 0; n < subnodes.size(); n++)
						{
                            complexnode = subnodes.at(n);
							CPtrList list;												
							DeserializeObject(property.GetFactory(), list, complexnode);

							CProperty newproperty(sPropName);
							newproperty.SetFactory(property.GetFactory());
							newproperty = list;
							pObj->SetPropertyValue(sPropName, newproperty);
						}												
						} // case ComplexList
						break;
					} // switch					
				}				
			}
			
            objList.push_back(pObj);
			nObjects++;
		}
	}
	factory->Destroy(pEmptyObject);
	return nObjects;
}
//------------------------------------------------------------------------------

// Returns number of items deserialized. In case of fatal error, returns 
// DESERIALIZATION_FAIL. Created objects are added to the objList
int CXMLSerializer::Deserialize(IObjectFactory* factory, CPtrList& objList)
{
    return DeserializeObject(factory, objList);
}

/**
 * @brief CXMLSerializer::selectSingleNode
 *        Applies the specified pattern-matching operation to this node's context and returns the first matching node.
 * @param refnode : Node where XPath expression will be executed against.
 * @param sXPath : A string specifying an XPath expression.
 * @return Returns the first node that matches the given pattern-matching operation.
 *         If no nodes match the expression, returns a null value
 */

QDomNode CXMLSerializer::selectSingleNode(QDomNode& refnode, const QString sXPath)
{
    QXmlQuery query;
    QString qres;
    query.setFocus(refnode.toDocument().toString());
    query.setQuery(sXPath);
    query.evaluateTo(&qres);

    qDebug() << sXPath;

    QDomDocument docQry;
    docQry.setContent(qres);

    return  docQry.firstChild();
}

/**
 * @brief CXMLSerializer::selectNodes
 *        Applies the specified pattern-matching operation to this node's context and returns the list of matching nodes
 * @param refnode : Node where XPath expression will be executed against.
 * @param sXPath : A string specifying an XPath expression.
 * @return Returns the collection of nodes selected by applying the given pattern-matching operation.
 *         If no nodes are selected, returns an empty collection.
 */
QDomNodeList CXMLSerializer::selectNodes(QDomNode& refnode, const QString sXPath)
{
    QXmlQuery query;
    QString qres;
    query.setFocus(refnode.toDocument().toString());
    query.setQuery(sXPath);
    query.evaluateTo(&qres);

    qDebug() << sXPath;

    QDomDocument docQry;
    docQry.setContent(qres);

    return  docQry.childNodes();
}
//------------------------------------------------------------------------------

// If a parentnode is supplied, it will create a new node and add it as a child
// node of the parent node. If parentnode is NULL, it will create a new node and
// return it. Imp: accept the returned node and Release it.
QDomElement CXMLSerializer::CreateChildNode(const QString& sNodeName,
                                                    QDomNode parent)
{	
    QDomElement node;
	
	_bstr_t		bsNode(sNodeName);
	_bstr_t		bsNamespace;

    node = m_doc.createElement(sNodeName);
		
    Q_ASSERT(!node.isNull());

    if (!parent.isNull())
	{
        parent.appendChild(node);
	}
	
	return node;
}
//------------------------------------------------------------------------------

// Pass a node pointer, property name & property value. e.g. <student> and 
// property is fname, will read text value from <fname> under student. 
// Also works for string lists.
bool CXMLSerializer::GetProperty(QDomNode node, CProperty& property)
{
    Q_ASSERT(!node.isNull());
    if (node.isNull())
	{
		return false;
	}

    QString						sNodeName;
    QDomNode 		propnode;	// property node
    QDomNodeList 	subnodes;	// only for lists
    QStringList				pList;		// only for lists
    QString						bsXPath;
    QString						bsValue;
    QString						sPropType;
    QString						sValue;
	PropertyType				type;

	ISerializable* pComplexProperty = NULL;
	type = property.GetType();

	switch(type)
	{
	case Simple:
        bsXPath = property.GetName();
        propnode = node.firstChildElement(bsXPath);

        if (propnode.isNull())
		{		
            qDebug() << "\nProperty node was not found.";
			return false;
		}
		
        //if (!GetAttributeValue(propnode, _T("Type"), sPropType))
		//	return false;

        bsValue = propnode.toElement().text();
        property	= bsValue;
		
		return true;

	case SimpleList:
		sNodeName = GetPlural(property.GetName());
		bsXPath = sNodeName;

        QXmlQuery query;
        QString qres;
        query.setFocus(node.toDocument().toString());
        query.setQuery(bsXPath);
        query.evaluateTo(&qres);

        QDomDocument docQry;
        docQry.setContent(qres);

        subnodes =  docQry.childNodes();
		
        pList = property.GetStringList();

        if (!subnodes.isEmpty())
		{
            QDomNode singlenode;
							
            for(int n = 0; n < subnodes.size(); n++)
			{
                singlenode = subnodes.item(n);
                bsValue =  singlenode.toElement().text();
                pList.push_back(bsValue);
			}
		}
		return true;
		break;
    }
    return false;
}
//------------------------------------------------------------------------------	

// Pass a node pointer, property name & property value. Will create the property
// node if required. e.g. <student> and property is fname, will create <fname>
// under student if needed and set the text value to the passed in value.
bool CXMLSerializer::SetProperty(QDomNode node,
								const CProperty& property,
								bool bCreateNewNode)
{
    Q_ASSERT (!node.isNull());
    if (node.isNull())
	{
		return false;
	}

    QString sProp = property.GetName();
    QDomNode		propnode;	// property node
    QString						sValue = property;
	_bstr_t						bsXPath(sProp);	
	_bstr_t						bsValue(sValue);
	
    if (false == bCreateNewNode)
	{
		// node existence checking is done only if the function was
		// told NOT to enforce new node creation
        propnode = node.firstChildElement(bsXPath);
	}

    if (propnode.isNull())
	{
		// this node does not exists, we need to create it
        QString		bsNode(sProp);

        propnode = m_doc.createElement(bsNode);
		
        Q_ASSERT(!propnode.isNull());
        if (propnode.isNull())
		{
			return false;
		}
		
        node.appendChild(propnode); // attach the node

		// this is not really required
		//SetAttributeValue(propnode, _T("Type"), property.GetTypeString());
	}
	
	
    propnode.setNodeValue(bsValue);

	return true;
}
//------------------------------------------------------------------------------

bool CXMLSerializer::SetAttributeValue(QDomNode& node,
                                  const QString& sAttName,
                                  const QString& sValue)
{
    Q_ASSERT(!node.isNull());
    if (node.isNull())
	{
		return false;
	}

	try
	{
        QDomNode attnode;
						
		_bstr_t		bsNode(sAttName);
		_bstr_t		bsValue(sValue);

        attnode = m_doc.createAttribute(bsNode);
			
        Q_ASSERT(!attnode.isNull());
        if (attnode.isNull())
		{
			return false;
		}
		
        attnode.setNodeValue(bsValue);
        node.attributes().setNamedItem(attnode);
		
		return true;	
	}
	catch(...)
	{
        qDebug() << UNSPEC_ERROR;
		return false;
	}
}
//------------------------------------------------------------------------------

bool CXMLSerializer::GetAttributeValue(QDomNode node,
                                       const QString& sAttName, QString& sValue)
{
    Q_ASSERT(!node.isNull());
    if (node.isNull())
	{
		return false;
	}

	try
	{
        QDomNode attnode;
        QDomNamedNodeMap attributes;
						
        QString		bsName(sAttName);
        QString		bsValue(sValue);

        attributes = node.attributes();

        if (attributes.isEmpty())
		{
			return false;
		}

        attnode = attributes.namedItem(bsName);
        if (attnode.isNull())
		{
            qDebug() << "Attribute " << sAttName << " not found.";
			return false;
		}
		
        sValue = attnode.toElement().text();
		
		return true;	
	}
	catch(...)
	{
        qDebug() << UNSPEC_ERROR;
		return false;
	}
}

//  ******** CXMLException Class *******
CXMLException::CXMLException(const QString& sMsg)
{
	m_sMessage = sMsg;
}
//------------------------------------------------------------------------------

CXMLException::CXMLException(const CXMLException& lhs)
{
	m_sMessage = lhs.m_sMessage;
}
//------------------------------------------------------------------------------

QString CXMLException::toString()
{
	return m_sMessage;
}
//------------------------------------------------------------------------------

bool CXMLException::GetErrorMessage(char * lpszError, unsigned int nMaxError,
                                    unsigned int* pnHelpContext)
{
    if ((int)nMaxError > m_sMessage.size())
	{
        strcpy(lpszError, m_sMessage.toLatin1());
		return TRUE;
	}
	else
	{
		return FALSE;
	}	
}
//------------------------------------------------------------------------------

QMessageBox::StandardButton CXMLException::ReportError(QMessageBox::Icon nType, unsigned int nMessageID)
{
    QString sMsg;

    switch (nType)
    {
    case QMessageBox::Critical:
        return QMessageBox::critical(0, "Critical", m_sMessage /*Translate error from nMessageID*/);
        break;
    case QMessageBox::Warning:
        return QMessageBox::warning(0, "Warning", m_sMessage /*Translate error from nMessageID*/);
        break;
    default:
        return QMessageBox::information(0, "Information", m_sMessage /*Translate error from nMessageID*/);
    }

}
//------------------------------------------------------------------------------
