/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef QJASPER_PRIVATE_H
#define QJASPER_PRIVATE_H

#include <qjasper.h>
#include <qjasper_util.h>

#include <jni.h>

#include <QList>
#include <QString>

JNIEnv* get_jvm();
bool show_report(const QString&, const QString&, QList<QRParameter> * listParam);

#endif // QJASPER_PRIVATE_H
