/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#include "settingsdlg.h"
#include "ui_settingsdlg.h"

#include <QSettings>

SettingsDlg::SettingsDlg(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDlg)
{
    ui->setupUi(this);

    ui->comboBoxDriver->addItems(QStringList() << "QMYSQL" << "QPSQL" << "QSQLITE");

    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    QString driver = settings.value("driver", QVariant("QSQLITE")).toString();
    ui->comboBoxDriver->setCurrentIndex(ui->comboBoxDriver->findText(driver));

    ui->lineEditHostname->setText(settings.value("hostname", QVariant("localhost")).toString());
    ui->lineEditDatabase->setText(settings.value("database", QVariant("orangehrm")).toString());
    ui->lineEditPort->setText(settings.value("port", QVariant("3309")).toString());
    ui->lineEditUsername->setText(settings.value("username", QVariant("root")).toString());
    ui->lineEditPassword->setText(settings.value("password", QVariant("mysql")).toString());
}

SettingsDlg::~SettingsDlg()
{
    delete ui;
}

void SettingsDlg::on_buttonBox_accepted()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    settings.setValue("driver", ui->comboBoxDriver->currentText());
    settings.setValue("hostname", ui->lineEditHostname->text());
    settings.setValue("database", ui->lineEditDatabase->text());
    settings.setValue("port", ui->lineEditPort->text());
    settings.setValue("username", ui->lineEditUsername->text());
    settings.setValue("password", ui->lineEditPassword->text());
}
