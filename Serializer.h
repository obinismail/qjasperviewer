/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */

//// Acknowledgement
// Filename		: Serializer.h
// Author		: Siddharth Barman
// Date			: 06 Aug 2005
// Description	: Declares interface ISerializer. This interface must be impleme
//				  nted by the class which wants to act as a serialization provid
//				  er. This class will be responsible for reading, writing and cr
//				  eating objects of classes which register with it.
//------------------------------------------------------------------------------

#ifndef Serializer_H
#define Serializer_H
#include "Serializable.h"
//------------------------------------------------------------------------------

class ISerializer
{
public:
	virtual				~ISerializer(){}	
	virtual bool		Serialize(ISerializable*) = 0;		// a single item
	virtual bool		Serialize(const CPtrList&) = 0;		// a list of items
	virtual int			Deserialize(IObjectFactory*, CPtrList& objList) = 0;
};

//------------------------------------------------------------------------------

#endif
