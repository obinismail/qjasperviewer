/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef UTIL_CLASS_H
#define UTIL_CLASS_H

#include <QString>
#include <QVariant>

/**
 * @brief The QRParameter struct
 * A reincarnation of JRParameter
 */

struct QRParameter
{
    const QString& name() { return pvt_name; }
    void setName(const QString& name) { pvt_name = name; }

    const QVariant& value() { return pvt_value; }
    void setValue(const QVariant& value) { pvt_value = value; }

    const QString& description() { return pvt_description; }
    void setValue(const QString& description) { pvt_description = description; }

    const QString className() { return pvt_class_name; }
    void setClassName(const QString& className) { pvt_class_name = className; }

    // Not Jasper implementation
    const QString& cBox_SQL() { return pvt_cbox_sql; }
    void setCBox_SQL(const QString sql) { pvt_cbox_sql = sql; }

    const QWidget * control() { return pvt_control; }
    void setControl(QWidget * control) { pvt_control = control; }

private:
    QString pvt_name;
    QVariant pvt_value;
    QString pvt_description;
    QString pvt_class_name;

    // Not Jasper implementation
    QString pvt_cbox_sql;
    QWidget * pvt_control;
};

#endif // UTIL_CLASS_H
