#!/bin/bash

# Use this script in QtCreator's 'Custom Process Step' project configuration to compile jni class

# Custom Process Step configurations are as follows:-
# Command : %{sourceDir}/build-jni.sh
# Working directory : %{sourceDir}
# Command argument %{buildDir}

cd ./jni
ant compile jar dist
cd ../
cp -rf ./dist/jni $1/

