/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdlg.h"
#include "jrxmldocument.h"
#include "settingsdlg.h"

#include <qjasper.h>

#include <QDebug>
#include <QFileDialog>
#include <QStandardItemModel>
#include <QStandardItem>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_settings( // QSettings::registerFormat("xml", readXmlFile, writeXmlFile),
               QSettings::UserScope,
               QCoreApplication::organizationName(),
               QCoreApplication::applicationName())
{
    m_pJasperManager = new QJasperManager(this);
    ui->setupUi(this);

    spawn_tableView();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::spawn_tableView()
{
    int size = m_settings.beginReadArray("JRXML");

    m_arr_jrxml.clear();

    for (int n=0; n < size; ++n)
    {
        m_settings.setArrayIndex(n);
        JRXML jrxml;
        jrxml.reportTitle = m_settings.value("title").toString();
        jrxml.reportPath = m_settings.value("path").toString();

        m_arr_jrxml.insert(jrxml);
    }
    m_settings.endArray();

    QStandardItemModel * model = new QStandardItemModel(m_arr_jrxml.size(), 2);

    QSet<JRXML>::iterator iter = m_arr_jrxml.begin();

    for (int n=0; n < m_arr_jrxml.size(), iter != m_arr_jrxml.end(); ++iter, ++n)
    {
        model->setItem(n, 0, new QStandardItem(iter->reportTitle));
        model->setItem(n, 1, new QStandardItem(iter->reportPath));
    }

    model->setHeaderData(0, Qt::Horizontal, QVariant("Report Title"));
    model->setHeaderData(1, Qt::Horizontal, QVariant("Report Location"));

    ui->tableView->setModel(model);
    ui->tableView->setColumnWidth(0,200);
    ui->tableView->setColumnWidth(1,350);
}

void Serialize(QString szXmlFile);
void Deserialize(QString szXmlFile);

void MainWindow::on_action_Register_triggered()
{
    JRXMLDocument doc;
    Serialize("/home/onn/JRXMLDocument.xml");

    QString lastFolder = m_settings.value("lastfolder").toString();
    QString selectedFile = QFileDialog::getOpenFileName(this,
                tr("Open JRXML"), lastFolder, tr("JRXML Files (*.xml *.jrxml)"));

    QFileInfo info(selectedFile);

    if (!selectedFile.isEmpty())
    {
        lastFolder = selectedFile;
        lastFolder.remove(info.fileName());
        m_settings.setValue("lastfolder", QVariant(QDir(selectedFile).path()));
    }

    if (info.exists())
    {
        QString filename = info.fileName();
        m_settings.beginWriteArray("JRXML");
        m_settings.setArrayIndex(m_arr_jrxml.size());
        m_settings.setValue("title", filename);
        m_settings.setValue("path", info.filePath());
        m_settings.endArray();
    }

    spawn_tableView();
}

void MainWindow::on_pushButtonRun_clicked()
{
    // Execute report selected from the tableView
    QModelIndex index = ui->tableView->currentIndex();

    if (index.isValid())
    {
        QStandardItemModel * pModel = (QStandardItemModel *)ui->tableView->model();

        QFileInfo fileInfo(pModel->index(index.row(), 1).data().toString());

        if (fileInfo.isReadable())
        {
            m_pJasperManager->runReport(fileInfo.absoluteFilePath());
        }
    }
}

void MainWindow::on_actionSe_ttings_triggered()
{
    SettingsDlg dlg;
    dlg.exec();
}

uint qHash(const JRXML& jrxml)
{
    return qHash(jrxml.reportTitle);
}

void MainWindow::on_action_About_triggered()
{
    AboutDlg dlg;
    dlg.exec();
}

void MainWindow::on_pushButtonDelete_clicked()
{
    // Execute report selected from the tableView
    QModelIndex index = ui->tableView->currentIndex();

    if (index.isValid())
    {
        // Remove from model
        QStandardItemModel * pModel = (QStandardItemModel *)ui->tableView->model();
        pModel->removeRow(index.row());

        // Remove from array
        m_arr_jrxml.erase(m_arr_jrxml.begin() + index.row());

        // Rewrite settings with the new array
        QSet<JRXML>::iterator iter = m_arr_jrxml.begin();

        m_settings.beginWriteArray("JRXML");
        m_settings.setArrayIndex(m_arr_jrxml.size());
        for (int n=0; n < m_arr_jrxml.size(), iter != m_arr_jrxml.end(); ++iter, ++n)
        {
            m_settings.setValue("title", iter->reportTitle);
            m_settings.setValue("path", iter->reportPath);
        }
        m_settings.endArray();
    }
}
