         QJasperViewer 0.1 GPL v3

         
QJasperViewer is a simple JRXML report runtime viewer. Written with C++ using
QtCreator with JasperSoft jasperreports library via JNI.


Changelog
---------

First release


Installation
------------

Apart from the installer, you need to install JDK6 to satisfy jasperreports
library during runtime.

Make sure 'javac' and 'java' are in the PATH environment variables.


Credit and Acknowledgement
--------------------------

a) JasperSoft www.jaspersoft.com
b) Qt from Digia


License Information
-------------------

Released under GNU Lesser V3 Public license.


Copyright 2013 Onn Khairuddin bin Rahmat
onn.khairuddin@gmail.com