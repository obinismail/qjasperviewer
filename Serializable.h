/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */

//// Acknowledgement
// Filename		: Serializable.h
// Author		: Siddharth Barman
// Date			: 06 Aug 2005
// Description	: Declares interface ISerializable. This interface must be imple
//				  mented by classes which want to be serializable. These classes
//				  must register with  a class implementing the ISerializer inter
//				  face. 
//				  Also declares IObjectFactory interface which is required to be
//				  implemented by the same class which implements ISerializable o
//				  r by a different class. The Serializer will make use of 
//				  IObjectFactory to create new instances of a class during de-se
//				  rialization.
//------------------------------------------------------------------------------
#ifndef Serializable_H
#define Serializable_H
//------------------------------------------------------------------------------

#include <QStringList>
#include <QVector>

typedef QVector<void *> CPtrList;

enum PropertyType
{
	Blank,
	Simple,
	SimpleList,
	Complex,
	ComplexList
};
//------------------------------------------------------------------------------
class CProperty;
class IObjectFactory;
//------------------------------------------------------------------------------

class ISerializable
{
public:
	virtual			~ISerializable(){};
	
    virtual int		GetProperties(QStringList& properties) = 0;

    virtual bool	GetPropertyValue(const QString& sProperty,
									 CProperty& sValue) = 0;

    virtual bool	SetPropertyValue(const QString& sProperty,
									 CProperty& sValue) = 0;

	virtual bool	HasMultipleInstances() = 0;									 
    virtual QString GetClassName() = 0;
    virtual QString GetID() = 0;
};
//------------------------------------------------------------------------------

// This class is a wrapper for a property which belongs to a class. It is capabl
// e of representing simple string property, an object implementing ISerializabl
// e, a list of string values and a list of ISerializable objects. Each property
// is identified by a property name (string).
class CProperty
{
public:
	
	CProperty(IObjectFactory* factory = NULL)
	{		
		m_object	= NULL;
		m_type		= Blank;
		m_factory	= factory;
	}

    CProperty(const QString& sName)
	{
		m_sName = sName;
	}

    CProperty(const QString& sName, const QString& sValue)
	{
		m_sName		= sName;
		m_sValue	= sValue;
		m_object	= NULL;
		m_type		= Simple;
	}

    CProperty(const QString& sName, ISerializable* object)
	{
		m_sName		= sName;
		m_object	= object;
		m_type		= Complex;
	}

	IObjectFactory* GetFactory() const
	{
		return m_factory;
	}

	void SetFactory(IObjectFactory* factory)
	{
		m_factory = factory;
	}

    QString GetName() const
	{
		return m_sName;
	}

    void SetName(const QString& sValue)
	{
		m_sName = sValue;
	}

    QString GetStringValue() const
	{
		return m_sValue;
	}

	ISerializable* GetObject() const
	{
		return m_object;
	}

    QStringList& GetStringList()
	{
		return m_values;
	}

	CPtrList& GetObjectList()
	{
		return m_objects;
	}

    CProperty& operator=(const QString& rhs)
	{
		m_sValue = rhs;
		m_object = NULL;
		m_type	 = Simple;
		return *this;
	}

    CProperty& operator=(const QStringList& rhs)
	{
		// Make a copy of the passed in string list
        QStringList tmp(rhs);
        m_values.swap(tmp);

		m_object = NULL;
		m_type	 = SimpleList;
		return *this;
	}

	CProperty& operator=(const CPtrList& rhs)
	{
		// Make a copy of the passed in string list

        CPtrList tmp(rhs);
        m_objects.swap(tmp);

		m_object = NULL;
		m_type	 = ComplexList;
		return *this;
	}

	CProperty& operator=(ISerializable* rhs)
	{
        m_sValue = "";
		m_object = rhs;
		m_type	 = Complex;
		return *this;
	}
	
    operator QString() const
	{
		return m_sValue;
	}

	operator ISerializable*() const
	{
		return m_object;
	}

	PropertyType GetType() const
	{
		return m_type;
	}
	
	void SetType(PropertyType type)
	{
		m_type = type;
	}

    QString GetTypeString() const
	{
        QString sType;

		switch(m_type)
		{
		case Blank:
            sType = "Blank";
			break;

		case Simple:
            sType = "Simple";
			break;

		case SimpleList:
            sType = "SimpleList";
			break;

		case Complex:
            sType = "Complex";
			break;

		case ComplexList:
            sType = "ComplexList";
			break;

		default:
			sType = "Blank";		
		}

		return sType;
	}

    static PropertyType Parse(const QString& sType)
	{
		PropertyType type = Blank;

        if(sType == "Simple")
			type = Simple;
        else if(sType == "SimpleList")
			type = SimpleList;
        else if(sType == "Complex")
			type = Complex;
        else if(sType == "ComplexList")
			type = ComplexList;
		
		return type;
	}	

private:
	PropertyType	m_type;
    QString			m_sName;
    QString			m_sValue;	// single simple (string )value
	ISerializable*	m_object;	// string complex value
    QStringList		m_values;	// list of simple (string) values
	CPtrList		m_objects;	// list of complex values
	IObjectFactory* m_factory;
};
//------------------------------------------------------------------------------

class IObjectFactory
{
public:
	virtual ISerializable* Create() = 0; // Factory function
	virtual void		   Destroy(ISerializable*) = 0;

};
//------------------------------------------------------------------------------

#endif
