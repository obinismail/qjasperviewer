/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QAbstractTableModel>

class TableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit TableModel(QObject *parent = 0);
    TableModel(QList< QPair<QString, QString> > listofPairs, QObject *parent=0);

     // int rowCount(const QModelIndex &parent) const;
     // int columnCount(const QModelIndex &parent) const;
     // QVariant data(const QModelIndex &index, int role) const;
     // QVariant headerData(int section, Qt::Orientation orientation, int role) const;
     // Qt::ItemFlags flags(const QModelIndex &index) const;
     // bool setData(const QModelIndex &index, const QVariant &value, int role=Qt::EditRole);
     // bool insertRows(int position, int rows, const QModelIndex &index=QModelIndex());
     // bool removeRows(int position, int rows, const QModelIndex &index=QModelIndex());
     // QList< QPair<QString, QString> > getList();
    
signals:
    
public slots:
    
private:
    QList< QPair<QString, QString> > listOfPairs;
};

#endif // TABLEMODEL_H
