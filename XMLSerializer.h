/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */

//// Acknowledgement
// Filename		: XMLSerializer.h
// Author		: Siddharth Barman
// Date			: Aug 06 2005
// Description	: Declares CXMLSerializer class implementing the ISerializer
//				  interface allowing it to act as a serialization provider. It
//				  provides serialization capability by making use of XML files
//				  for persistence.
//------------------------------------------------------------------------------

#ifndef XMLSerializer_H
#define XMLSerializer_H

#include "Serializer.h"
#include <QDomDocument>
#include <QMessageBox>

#define UNSPEC_ERROR "Unspecified error."
#define MB_OK 0

//------------------------------------------------------------------------------
#define DESERIALIZATION_FAIL	-1

class CXMLSerializer : public ISerializer
{
public:
	CXMLSerializer();
    CXMLSerializer(const QString& sFile, const QString& sAppName, bool bLoad);
	~CXMLSerializer();

	void	ReadFile();

    void	SetFile(const QString&);
    QString GetFile();

    void	SetApplicationName(const QString&);
    QString GetApplicationName();

	bool	Serialize(ISerializable*);		// a single item
	bool	Serialize(const CPtrList&);		// a list of items
	int		Deserialize(IObjectFactory*, CPtrList& objList); // list of items [out]

    QDomNode selectSingleNode(QDomNode& refnode, const QString sXPath);
    QDomNodeList selectNodes(QDomNode &refnode, const QString sXPath);

private:
    QString m_sFile;
    QString m_sAppName;
    QDomDocument m_doc;

    bool GetProperty(QDomNode, CProperty&);
								  // specify the name in the property object itself
	
    bool SetProperty(QDomNode, const CProperty& property,
					 bool bCreateNewNode = false);
	
    bool SetAttributeValue(QDomNode&, const QString& sAttName,
                           const QString& sValue);
	
    bool GetAttributeValue(QDomNode node, const QString& sAttName,
                           QString& sValue);
	
    bool SerializeObject(ISerializable*, QDomNode parent = QDomNode());

	int DeserializeObject(IObjectFactory*, CPtrList& objList, 
                          QDomNode parent = QDomNode());
	
    QDomElement CreateChildNode(const QString& sNodeName,
                                QDomNode parent = QDomNode());

    QString GetPlural(const QString&);
};
//------------------------------------------------------------------------------

class CXMLException
{
private:
    QString m_sMessage;

public:
    CXMLException(const QString& sMsg);
	CXMLException(const CXMLException&);
    QString toString();
    bool GetErrorMessage(char * lpszError, unsigned int nMaxError,
                         unsigned int * pnHelpContext = NULL);
    QMessageBox::StandardButton ReportError(QMessageBox::Icon nType = QMessageBox::Information, unsigned int nMessageID = 0);
};

#endif
