/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef PARAMETERDLG_H
#define PARAMETERDLG_H

#include <QDialog>
#include "qjasper_util.h"
namespace Ui {
class ParameterDlg;
}

class ParameterDlg : public QDialog
{
    Q_OBJECT
    
public:
    explicit ParameterDlg(const QString& fileName, QList<QRParameter> * plistParam, QWidget *parent = 0);
    ~ParameterDlg();

    const QString& outputFormat() {return pvt_OutputFormat;}
    
private slots:
    void on_buttonBox_accepted();

    void ON_RadioClicked_clicked();

private:
    bool parse();
    bool constructDlg();
    Ui::ParameterDlg *ui;

    const QString& pvt_FileName;
    QList<QRParameter> * m_listParam;
    QString pvt_OutputFormat;
};

#endif // PARAMETERDLG_H
