/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QSet>

#include "qjasper.h"

class JRXML{
public:
    QString reportTitle;
    QString reportPath;

    bool operator==(const JRXML& other) const { return this->reportTitle == other.reportTitle; }
};

uint qHash(const JRXML& jrxml);

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void on_action_Register_triggered();

    void spawn_tableView();

    void on_pushButtonRun_clicked();

    void on_actionSe_ttings_triggered();

    void on_action_About_triggered();

    void on_pushButtonDelete_clicked();

private:
    Ui::MainWindow *ui;
    QSettings m_settings;
    QSet<JRXML> m_arr_jrxml;

    QJasperManager * m_pJasperManager;
};

#endif // MAINWINDOW_H
