/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
#include "parameterdlg.h"
#include "ui_parameterdlg.h"

#include <QDebug>
#include <QFile>
#include <QXmlStreamReader>

#include <QLabel>
#include <QLineEdit>
#include <QDateTimeEdit>
#include <QComboBox>
#include <QHBoxLayout>

#include <QSettings>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlField>

ParameterDlg::ParameterDlg(const QString& fileName, QList<QRParameter> * plistParam, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ParameterDlg),
    pvt_FileName(fileName),
    m_listParam(plistParam),
    pvt_OutputFormat("PDF")
{
    ui->setupUi(this);

    connect (ui->radiobtnPDF, SIGNAL(clicked()), this, SLOT(ON_RadioClicked_clicked()));
    connect (ui->radiobtnXLS, SIGNAL(clicked()), this, SLOT(ON_RadioClicked_clicked()));
    connect (ui->radiobtnCSV, SIGNAL(clicked()), this, SLOT(ON_RadioClicked_clicked()));
    connect (ui->radiobtnXML, SIGNAL(clicked()), this, SLOT(ON_RadioClicked_clicked()));

    ui->radiobtnPDF->setChecked(true);
    this->setWindowTitle(pvt_FileName);

    parse();
    constructDlg();
}

ParameterDlg::~ParameterDlg()
{
    delete ui;
}

bool ParameterDlg::parse()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());

    // Load the JRXML file
    QFile file(pvt_FileName);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "Failed to open file " << pvt_FileName;
        return false;
    }
    // Get the PARAMETER argument
    QXmlStreamReader xml(&file);

    while(!xml.atEnd() && !xml.hasError())
    {
        // Read next element
        QXmlStreamReader::TokenType token = xml.readNext();
        // If token is just start element, go to next
        if (token == QXmlStreamReader::StartDocument)
            continue;

        if (token == QXmlStreamReader::StartElement)
        {
            if(xml.name() == "parameter")
            {
                // Dig in
                QXmlStreamAttributes attr = xml.attributes();

                // Do not prompt parameter not for prompting
                if (attr.value("isForPrompting") == "false")
                    continue;

                QRParameter param;
                param.setName(attr.value("name").toLatin1());
                param.setClassName(attr.value("class").toLatin1());

                while (xml.readNextStartElement())
                {
                    if (xml.name() == "parameterDescription")
                    {
                        // Read CData section assuming they are SQL query
                        xml.readNext();
                        {
                            QString sql = xml.text().toString();
                            QSqlDatabase db = QSqlDatabase::addDatabase(settings.value("driver").toString(), "TEMP_PARAMDLG");
                            db.setDatabaseName(settings.value("database").toString());
                            db.setHostName(settings.value("hostname").toString());
                            db.setUserName(settings.value("username").toString());
                            db.setPassword(settings.value("password").toString());
                            db.open();

                            /* We use parameterDescription as source of combobox item.
                             * The description must provide SQL Query with two field 'ID' and 'Name'
                             * Query here is for testing, thus limit of 1 record only.
                             */

                            QSqlQuery qry = db.exec(sql + "limit 1");
                            if (qry.next())
                            {
                                qDebug() << "Found required field for combo box list :" << qry.record().fieldName(0) << " and " << qry.record().fieldName(1);
                                if (qry.record().fieldName(0).toUpper() == "ID" && qry.record().fieldName(1).toUpper() == "NAME")
                                    param.setCBox_SQL(sql);
                                else
                                    param.setCBox_SQL("");
                            }
                            else
                                param.setCBox_SQL("");
                            qry.finish();
                            db.close();
                            QSqlDatabase::removeDatabase("TEMP_PARAMDLG");
                        }
                    }
                }

                m_listParam->append(param);
            }
            else
                continue;
        }

    }
    xml.clear();
    file.close();
    return true;
}

bool ParameterDlg::constructDlg()
{
    QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
    // Iterate parameter list and create the control

    QList<QRParameter>::Iterator iter = m_listParam->begin();

    for (; iter != m_listParam->end(); ++iter)
    {
        QHBoxLayout * layout = new QHBoxLayout();

        char szName[256];
        ::memset(szName, 0, 256);
        ::strcpy(szName, iter->name().toLower().toLatin1());

        if (*szName >= 'a')
            *szName -= 32;

        QLabel * label = new QLabel(szName);
        QWidget * editor = 0;

        if (iter->className() == "java.lang.String")
        {
            if (!iter->cBox_SQL().isEmpty())
            {
                QComboBox * cb = new QComboBox();

                QSqlDatabase db = QSqlDatabase::addDatabase(settings.value("driver").toString(), "TEMP_CBOX");
                db.setDatabaseName(settings.value("database").toString());
                db.setHostName(settings.value("hostname").toString());
                db.setUserName(settings.value("username").toString());
                db.setPassword(settings.value("password").toString());
                db.open();

                QSqlQuery qry = db.exec(iter->cBox_SQL());
                while (qry.next())
                    cb->addItem(qry.record().field(1).value().toString(), qry.record().field(0).value());

                qry.finish();
                db.close();

                // QSqlDatabase::removeDatabase("TEMP_CBOX");
                editor = cb;
            }
            else
                editor = new QLineEdit();
        }
        else
            if (iter->className() == "java.util.Date")
            {
                editor = new QDateTimeEdit();
                QDateTimeEdit *d = (QDateTimeEdit*)editor;
                d->setDisplayFormat("dd/MM/yyyy");
            }

        layout->addWidget(label);
        layout->addWidget(editor);

        ui->vertLayout->addLayout(layout);
        label->setMinimumWidth(100);
        iter->setControl(editor);
    }
    return true;
}

void ParameterDlg::on_buttonBox_accepted()
{
    QList<QRParameter>::Iterator iter = m_listParam->begin();

    for (; iter != m_listParam->end(); ++iter)
    {
        if( iter->className() == "java.lang.String")
        {
            if (!iter->cBox_SQL().isEmpty())
            {
                QComboBox * cb = (QComboBox*)iter->control();
                iter->setValue(cb->itemData(cb->currentIndex()));
            }
            else
                iter->setValue(((QLineEdit*)iter->control())->text());
        }

        if (iter->className() == "java.util.Date")
        {
            iter->setValue (((QDateTimeEdit*)iter->control())->text());
        }
    }
}

void ParameterDlg::ON_RadioClicked_clicked()
{
    ui->radiobtnPDF->isChecked() ? pvt_OutputFormat = "PDF" : "";
    ui->radiobtnXLS->isChecked() ? pvt_OutputFormat = "XLS" : "";
    ui->radiobtnCSV->isChecked() ? pvt_OutputFormat = "CSV" : "";
    ui->radiobtnXML->isChecked() ? pvt_OutputFormat = "XML" : "";
}
