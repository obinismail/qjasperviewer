/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qjasper_util.h"
#include "qjasper_private.h"
#include "parameterdlg.h"

#include <jni.h>

#include <QCoreApplication>
#include <QDebug>
#include <QDesktopServices>
#include <QDir>
#include <QMessageBox>
#include <QSettings>
#include <QUrl>

#ifdef Q_WS_WIN
QString vm_arg_sep = ";";
#else
QString vm_arg_sep = ":";
#endif

JNIEnv *env = NULL;
JavaVM *jvm = NULL;

struct JDBCDriver
{
    JDBCDriver (const QString& qtDriver)
        : PGSQL_DRV ("org.postgresql.Driver")
        , MYSQL_DRV ("com.mysql.jdbc.Driver")
        , SQLITE_DRV ("org.sqlite.JDBC")
        , PGSQL_URL ("jdbc:postgresql://")
        , MYSQL_URL ("jdbc:mysql://")
        , SQLITE_URL ("jdbc:sqlite://")
    {
        m_qtDriver = qtDriver;
    }

    const QString url()
    {
        if ( m_qtDriver == "QPSQL")
            return PGSQL_URL;
        else if ( m_qtDriver == "QMYSQL")
            return MYSQL_URL;
        else if ( m_qtDriver == "QSQLITE")
            return SQLITE_URL;

        QMessageBox::critical(NULL, "Database not supported", "Only Postgresql, Mysql and SQLite database are supported");
        return "";
    }
    const QString driver()
    {
        if ( m_qtDriver == "QPSQL")
            return PGSQL_DRV;
        else if ( m_qtDriver == "QMYSQL")
            return MYSQL_DRV;
        else if ( m_qtDriver == "QSQLITE")
            return SQLITE_DRV;

        QMessageBox::critical(NULL, "Database not supported", "Only Postgresql, Mysql and SQLite database are supported");
        return "";
    }

    private:
        QString m_qtDriver;
        const QString PGSQL_DRV;
        const QString MYSQL_DRV;
        const QString SQLITE_DRV;

        const QString PGSQL_URL;
        const QString MYSQL_URL;
        const QString SQLITE_URL;
};



QJasperManager::QJasperManager(QObject *parent)
{
    setParent(parent);
}

QJasperManager::~QJasperManager()
{
    if(jvm != NULL)
        jvm->DestroyJavaVM();
    env = NULL;
}

/**
 * @brief QJasperManager::runReport
 * @param reportFilePath Absolute path and file
 * @return
 */
bool QJasperManager::runReport(const QString& reportFilePath)
{
    QList<QRParameter> listParam;
    ParameterDlg dlg(reportFilePath, &listParam, 0);
    if(dlg.exec() == QDialog::Accepted)
    {
        show_report(reportFilePath, dlg.outputFormat(), &listParam);
        return !reportError();
    }
    return false;
}

bool QJasperManager::reportError()
{
    JNIEnv* penv = get_jvm();
    while(penv && penv->ExceptionCheck())
    {
        jthrowable e = penv->ExceptionOccurred();

        penv->ExceptionClear();

        jclass eclass = penv->GetObjectClass(e);

        jmethodID mid = penv->GetMethodID(eclass, "toString", "()Ljava/lang/String;");

        jstring jErrorMsg = (jstring) penv->CallObjectMethod(e, mid);
        const char* cErrorMsg = penv->GetStringUTFChars(jErrorMsg, NULL);

        // strcpy(buf, cErrorMsg);
        QMessageBox::critical(0, "Java Error", QString(cErrorMsg));
        qDebug() << cErrorMsg;

        penv->ReleaseStringUTFChars(jErrorMsg, cErrorMsg);

        return true;
    }
    return false; // no error
}


JNIEnv* get_jvm()
{
    if(env == NULL)
    {
        // Prepare list of jar files for classpath

        QString jarFiles = "-Djava.class.path=.";

        qDebug() << qApp->applicationDirPath();

        QDir libDir("jni/lib");
        QStringList list = libDir.entryList(QDir::Files);
        foreach (QString s, list) {
            jarFiles += vm_arg_sep + "jni/lib/";
            jarFiles += s;
        }
        qDebug() << jarFiles;

        char szJar[jarFiles.size()];
        memset(szJar, 0, jarFiles.size());
        strcpy(szJar, jarFiles.toLatin1());


        JavaVMInitArgs vm_args;
        JavaVMOption options[2];

        memset(&options, 0, sizeof(options));

        //Path to the java source code
        options[0].optionString = szJar;
        options[1].optionString = "-Xms=64m -Xmx=512m";


        vm_args.version = JNI_VERSION_1_6; //JDK version. This indicates version 1.6
        vm_args.nOptions = 1;
        vm_args.options = options;
        vm_args.ignoreUnrecognized = JNI_FALSE;

        int ret = JNI_CreateJavaVM(&jvm, (void **)&env, &vm_args);
        if(ret < 0)
        {
            printf("\nUnable to Launch JVM\n");
            return 0;
        }
    }
    return env;
}

bool show_report(const QString& reportName, const QString& outputFormat, QList<QRParameter>* listParam)
{
    // Execute Jasperreports via JNI

    JNIEnv *penv = get_jvm();
    jstring resultPath = 0;

    if(!penv)
    {
        return false;
        qDebug() << "Unable to load Java Virtual Machine.";
    }

    jclass JasperClass = penv->FindClass("my/qjasper/GenerateReports");
    jmethodID generateReport = NULL; // public static int generateReport(String reportName)

    if(JasperClass)
    {
        generateReport = penv->GetStaticMethodID(JasperClass, "generateReport",
            "(Ljava/lang/String;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");

        if(generateReport)
        {
            QSettings settings(QCoreApplication::organizationName(), QCoreApplication::applicationName());
            QString strQtDriver = settings.value("driver").toString();

            jstring StringDriver = penv->NewStringUTF(JDBCDriver(strQtDriver).driver().toLatin1());
            jstring StringConnection = penv->NewStringUTF(QString(JDBCDriver(strQtDriver).url()
                                                         + settings.value("hostname").toString()
                                                         + "/" + settings.value("database").toString()
                                                         + "?user=" + settings.value("username").toString()
                                                         + "&password=" + settings.value("password").toString()).toAscii());
            jstring StringOutputFormat = penv->NewStringUTF(outputFormat.toAscii());
            jstring StringFilename = penv->NewStringUTF(reportName.toAscii());

            // Create java.util.HashMap
            jclass mapClass = penv->FindClass("java/util/HashMap");

            if (mapClass)
            {
                jsize map_len = 1;

                jmethodID init = penv->GetMethodID(mapClass, "<init>", "(I)V");
                jobject hashMap = penv->NewObject(mapClass, init, map_len);

                jmethodID put = penv->GetMethodID(mapClass, "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");

                if (put)
                {
                    if (listParam)
                    {
                        QList<QRParameter>::Iterator iter = listParam->begin();
                        for (; iter != listParam->end(); ++iter)
                        {
                            jstring paramName = penv->NewStringUTF(iter->name().toAscii());
                            jobject paramValue = 0;

                            if (iter->className() == "java.util.Date")
                            {
                                /*Let java file handle conversion to the correct type.*/
                                paramValue = penv->NewStringUTF(QString("DATE_" + iter->value().toString()).toAscii());
                            }
                            else // java.util.String
                                paramValue = penv->NewStringUTF(iter->value().toString().toAscii());

                            penv->CallObjectMethod(hashMap, put, paramName, paramValue);

                            penv->DeleteLocalRef(paramName);
                            penv->DeleteLocalRef(paramValue);
                        }
                    }// if (listParam)

                    // Call the function now
                    resultPath = (jstring)penv->CallStaticObjectMethod(
                                JasperClass, generateReport, StringFilename, hashMap, StringOutputFormat, StringConnection, StringDriver);
                }
                // Prevent memory leak
                penv->DeleteLocalRef(StringFilename);
                penv->DeleteLocalRef(StringConnection);
                penv->DeleteLocalRef(StringDriver);
                penv->DeleteLocalRef(StringOutputFormat);
                penv->DeleteLocalRef(mapClass);
            } // if (mapClass)
            else
                return false;

        } // if (generateReport)
        else
            return false;

    } // if (JasperClass
    else
        return false;

    if(resultPath)
    {
        bool br = false;
        jboolean isCopy = JNI_FALSE;
        const char* cResult = penv->GetStringUTFChars(resultPath, &isCopy);
        char buf[1024];
        ::memset(buf, 0, 1024);
        strcpy(buf, cResult);
        penv->ReleaseStringUTFChars(resultPath, cResult);

        if (QString(buf) != "FALSE")
            QDesktopServices::openUrl(QUrl(QString("file:///") + buf, QUrl::TolerantMode));
        else
            return false;
    }
    return true;
}


