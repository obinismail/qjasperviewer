/*
 * QJasperViewer - Free JRXML Runtime Viewer.
 * Copyright (C) 2013 Onn Khairuddin bin Rahmat. All rights reserved.
 *
 * QJasperViewer is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QJasperViewer is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QJasperViewer. If not, see <http://www.gnu.org/licenses/>.
 */
package my.qjasper;

import java.io.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.JRXmlExporter;

public class GenerateReports {
	
	public static void main(String ars[]) {
		GenerateReports r = new GenerateReports();
	}

	/* String generateReport
	 * Input 
	 * : jrxml_file : Full path to the report file
	 * Return Full path to generated report file
	 */
	public static String generateReport(String jrxml_file, HashMap<String, Object> paramI, String outputFormat, String connUrl, String connDriver) {
		String returnResult = "";
                String tempFolder = System.getProperty("java.io.tmpdir") + "/";
		try {
			Class.forName(connDriver);
                        Connection conn = DriverManager.getConnection(connUrl);

			// Read jrxml file			
			InputStream input = new FileInputStream(new File(jrxml_file));
			JasperDesign jasperDesign = JRXmlLoader.load(input);

			// Compile the report
			JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

			Map<String, Object> param = paramI;
                        Iterator iter = paramI.entrySet().iterator();
                        while (iter.hasNext())
                        {
                            Map.Entry pairs = (Map.Entry)iter.next();
                            
                            if (pairs.getValue().toString().contains("DATE_"))
                            {
                                String strDate = pairs.getValue().toString().replace("DATE_", "");
				Date d = new SimpleDateFormat("dd/MM/yyyy").parse(strDate);
				paramI.put(pairs.getKey().toString(), d);     
                            }
                        }

			// Filling Report to File
			JasperPrint print = JasperFillManager.fillReport(jasperReport, param, conn);
                        OutputStream outStream = null;

			//Exporting the report
			if (outputFormat.compareToIgnoreCase("XML") == 0)
			{
                                returnResult = tempFolder + "readonly.xml";
				ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
                                outStream = new FileOutputStream(new File(returnResult));

				JRXmlExporter xmlExporter = new JRXmlExporter();
				xmlExporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
				xmlExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputByteArray);
				xmlExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
				xmlExporter.exportReport();
                                outStream.write(outputByteArray.toByteArray());

			} else
			if (outputFormat.compareToIgnoreCase("CSV") == 0)
			{
                                returnResult = tempFolder + "readonly.csv";
				ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
                                outStream = new FileOutputStream(new File(returnResult));

				//byte-order marker (BOM)
				byte b[] = {(byte)0xEF, (byte)0xBB, (byte)0xBF};
				//insert BOM byte array into outputStream
                                outStream.write(b);
				JRExporter csvExporter = new JRCsvExporter();
				csvExporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
				csvExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputByteArray);
				csvExporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "UTF-8");
				csvExporter.exportReport();
                                outStream.write(outputByteArray.toByteArray());
			} else 
			if (outputFormat.compareToIgnoreCase("XLS") == 0)
			{
                                returnResult = tempFolder + "readonly.xls";
				ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
                                outStream = new FileOutputStream(new File(returnResult));

				JRXlsExporter exporterXLS = new JRXlsExporter();
         			exporterXLS.setParameter(JRXlsExporterParameter.JASPER_PRINT, print);
			        exporterXLS.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputByteArray);
			        exporterXLS.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
         			exporterXLS.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
         			exporterXLS.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
         			exporterXLS.exportReport();
                                outStream.write(outputByteArray.toByteArray());
			} else // PDF by default
			{	
                                returnResult = tempFolder + "readonly.pdf";
                                outStream = new FileOutputStream(new File(returnResult));
                                JasperExportManager.exportReportToPdfStream(print, outStream);
			}

			// Report Generation Complete
                        outStream.flush();
                        outStream.close();
			conn.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "FALSE";
		} catch (JRException e) {
			e.printStackTrace();
			return "FALSE";
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return "FALSE";
		} catch (ParseException e) {
			e.printStackTrace();
			return "FALSE";
		} catch (SQLException e) {
			e.printStackTrace();
			return "FALSE";
		} catch (IOException e) {
			e.printStackTrace();
			return "FALSE";
		}
		return returnResult;
	}
}
